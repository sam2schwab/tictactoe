﻿namespace TicTacToe
{
    public enum Symbol { None, X, O }
    public struct Move
    {
        public Move(int row, int column, Symbol symbol)
        {
            Row = row;
            Column = column;
            Symbol = symbol;
        }
        public int Row;
        public int Column;
        public Symbol Symbol;
    }
    public static class Extensions
    {
        public static char ToChar(this Symbol s)
        {
            switch (s)
            {
                case Symbol.X:
                    return 'X';
                case Symbol.O:
                    return 'O';
                default:
                    return ' ';
            }
        }
    }
}

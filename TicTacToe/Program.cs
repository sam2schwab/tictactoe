﻿using System;

namespace TicTacToe
{
    static class Program
    {
        static void Main()
        {
            var game = new Game();
            game.Start();
            Console.ReadKey();
        }
    }
}

﻿using System;

namespace TicTacToe
{
    public class Game
    {
        private Board Board { get; set; }
        private Player[] Players { get; set; }
        private ConsoleColor BoardColor = ConsoleColor.White;

        public Game()
        {
            Board = new Board();
            Players = new Player[] 
            { 
                new HumanPlayer(Symbol.X,ConsoleColor.Blue, Board),
                new HumanPlayer(Symbol.O,ConsoleColor.Green, Board)
            };
        }

        public void Start()
        {
            Display();
            bool end = false;
            while (!end)
            {
                foreach (Player p in Players)
                {
                    p.Play();
                    Display();
                    if (CheckVictory(p.Symbol))
                    {
                        Write("Victoire du joueur " + (int)p.Symbol, p.Color);
                        end = true;
                        break;
                    }
                }
            }
        }
        public static void WriteError(string error)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(error);
        }

        private void Display()
        {
            for (int i = 0; i < 3; i++)
            {
                PrintLine(i);
                if (i == 2) break;
                Console.WriteLine("---|---|---");
            }
        }
        private void PrintLine(int lineNb)
        {
            for (int i = 0; i < 3; i++)
            {
                Write(" " + Board[lineNb, i].ToChar() + " ",
                    Board[lineNb, i] == Players[0].Symbol ? Players[0].Color : Players[1].Color);
                if (i == 2) break;
                Write("|", BoardColor);
            }
            Console.ForegroundColor = BoardColor;
            Console.WriteLine();
        }
        private void Write(string s, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.Write(s);
        }
        private bool CheckVictory(Symbol s)
        {
            if(Board[1,1] == s)
            {
                if (Board[0, 0] == s && Board[2, 2] == s ||
                    Board[2, 0] == s && Board[0, 2] == s ||
                    Board[1, 0] == s && Board[1, 2] == s ||
                    Board[0, 1] == s && Board[2, 1] == s) return true;
            }
            if(Board[0,0] == s)
            {
                if (Board[0, 1] == s && Board[0, 2] == s ||
                    Board[1, 0] == s && Board[2, 0] == s) return true;
            }
            if(Board[2,2] == s)
            {
                if (Board[2, 1] == s && Board[2, 0] == s ||
                    Board[0, 2] == s && Board[1, 2] == s) return true;
            }
            return false;
        }
    }
}

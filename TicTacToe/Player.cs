﻿using System;

namespace TicTacToe
{
    public abstract class Player
    {
        public Symbol Symbol { get; private set; }
        public ConsoleColor Color { get; private set; }
        public Board Board { get; private set; }

        public Player(Symbol symbol, ConsoleColor color, Board board)
        {
            Symbol = symbol;
            Color = color;
            Board = board;
        }

        public abstract void Play();

        protected void Write (string msg, params object[] args)
        {
            Console.ForegroundColor = Color;
            msg = "Player " + (int)Symbol + " - " + msg;
            Console.Write(msg,args);
        }
    }
}

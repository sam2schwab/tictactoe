﻿using System;

namespace TicTacToe
{
    class AIPlayer : Player
    {
        public AIPlayer(Symbol symbol, ConsoleColor color, Board board) : base(symbol, color, board)
        {
        }

        public override void Play()
        {
            
        }
    }
}
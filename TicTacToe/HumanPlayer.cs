using System;

namespace TicTacToe
{
    public class HumanPlayer : Player
    {
        public HumanPlayer(Symbol symbol, ConsoleColor color, Board board) : base(symbol, color, board) { }

        public override void Play()
        {
            Move move = new Move() { Symbol = Symbol };
            do
            {
                move.Row = ReadCoord("Row");
                move.Column = ReadCoord("Column");
            } while (!IsMoveValid(move, Board));
            Board[move.Row, move.Column] = move.Symbol;
        }

        private int ReadCoord(string prompt)
        {
            int coord;
            string key;
            Write("{0}: ", prompt);
            key = Console.ReadKey().KeyChar.ToString();
            while (!int.TryParse(key, out coord) || coord > 2)
            {
                Game.WriteError("Invalid Coordinate, please enter an integer between 0 and 2");
                Write("{0}: ", prompt);
                key = Console.ReadKey().KeyChar.ToString();
            }
            Console.WriteLine();
            return coord;
        }
        private bool IsMoveValid(Move move, Board board)
        {
            bool isValid = board[move.Row, move.Column] == Symbol.None;
            if (!isValid)
                Game.WriteError("Invalid move, you can only place a piece where there is an empty space");
            return isValid;
        }
    }
}
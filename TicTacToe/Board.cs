﻿using System;

namespace TicTacToe
{
    public class BoardChangedEventArgs : EventArgs
    {
        public BoardChangedEventArgs(Move move)
        {
            Move = move;
        }
        public Move Move { get; set; }
    }
    public class Board
    {
        private readonly Symbol[,] board = new Symbol[3,3];

        public event EventHandler<BoardChangedEventArgs> Changed;  

        public Symbol this[int index0, int index1]
        {
            get { return board[index0, index1]; }
            set
            {
                board[index0, index1] = value;
                if (Changed != null) Changed(this, new BoardChangedEventArgs(new Move(index0, index1, value)));
            }
        }
    }
}
